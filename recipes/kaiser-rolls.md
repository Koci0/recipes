---
author:
  name: "King Arthur Baking"
  source: ""
title: Kaiser Rolls
tags:
  - baking
  - bread
---

# Kaiser rolls

## Dough 

- 361g flour
- 3/2t sugar
- 5/4t / 8g salt
- 1 egg
- 28g butter, soft
- 170g water, lukewarm

Knead everything. Rise for 1h.

Shape into rolls. **Optionally** brush with milk and sprinkle toppings. Rise 45-60min.

Bake at 220C for 15-17min with steam.
