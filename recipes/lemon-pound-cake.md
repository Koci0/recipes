---
author:
  name: ""
  source: ""
title: Lemon Pound Cake
tags: 
  - baking
  - desert
---

# Lemon Pound Cake

## Dry

- 375g flour
- 1/2t baking soda
- 1/2t salt

## Wet

- 240ml buttermilk
- 2T lemon zest
- 2T lemon juice

## Dough

- 230g butter, soft
- 220g sugar
- 3 eggs

Cream together. Add *dry* and *wet* (1/4 dry, 1/3 wet, ...). Grease the pan and coat with sugar. Bake at 165C for 65-75min.  
Remove after 10 minutes.
