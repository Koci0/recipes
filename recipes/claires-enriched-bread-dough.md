---
author:
  name: "Claire Saffitz"
  source: "Desert Person/New York Times@YT"
title: Claire's Enriched Bread Dough
tags: 
  - baking
  - bread
---

# Claire's Enriched Bread Dough

## Tanhzhong

* 240g milk or buttermilk
* 50g flour

Mix in saucepan until gel-like. **Chill**.

---

## Dough

* 4 eggs + **extra** for eggwash
* 2T oil
* 50g sugar
* 10g salt
* 2t/6g yeast
* 490g flour
* 113g butter, chilled and cubed

Knead eggs, oil, sugar, salt, yeast and flour with *thangzong* for 12-15min.  
Add half of butter, knead for 5min on low. Repeat with the rest of butter.  
Fold and rise 30min in room temp and 4-24h in the fridge.

Shape, proof for 1.5-2h.

Eggwash. Bake at 180C for 25-30min.
