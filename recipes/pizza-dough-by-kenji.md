---
author:
  name: "J. Kenji López-Alt"
  source: "https://www.seriouseats.com/recipes/2012/07/basic-new-york-style-pizza-dough.html"
title: Pizza Dough by Kenji
tags:
  - baking
  - bread
---

# Pizza Dough by Kenji

## Dough

- 630g flour
- 15g sugar
- 1T (10g) salt
- 2t (10g) instant yeast
- 32g olive oil
- 420g water, warm

Knead. Rise in the fridge at least 1 day, up to 5.

Rest at room temperature for at least 2 hours before baking.
