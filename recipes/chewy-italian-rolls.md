---
author:
  name: "King Arthur Baking"
  source: ""
title: Chewy Italian Rools
tags:
  - baking
  - bread
---

# Chewy Italian Rolls

## Starter

- 117g flour
- 28g whole-wheat
- 227g water, lukewarm
- 1/8t yeast

Mix and rise for 12-20h.

## Dough

- 298g flour
- 113g water
- 2t/12g sugar
- 1/2t yeast

Knead everything with *starter*. Rise for 1-2h.

Divide in 8, shape flour side up. Rise for 45min.

Bake at 230C for 13-15min.
