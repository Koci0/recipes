---
author:
  name: ""
  source: ""
title: Challah
tags: 
  - baking
  - bread
---

# Challah

## Dough

* 6g yeast
* 7g sugar
* 220g water, lukewarm
* 60g oil
* 2 eggs
* 15g honey
* 50g sugar
* 8g salt
* 400g + 300g (=700g) flour

Bloom yeast with sugar and water. Knead everything with enough flour to hold its shape. Rise until doubled (about 1h).

Punch down and rise until doubled (about 1h).

Divide in 8 pieces. Make loaf with 4 strands. Eggwash. Rise for 1h.

Eggwash and **optionally** sprinkle on toppings. Bake at 180C for 25-35min (87C internally).
