---
author:
  name: ""
  source: ""
title: Sourdough Bread
tags: 
  - baking
  - bread
---

# Sourdough bread

## Dough

- 400g bread flour
- 100g whole wheat
- 300g water (60%)
- 100g starter (20%)
- 10g salt (2%)

Autolyse 12h.

Mix starter and salt. Mix and rest for 15min. Remove sample to monitor raise. Knead.  
Proof until size increases 20-40% - during that stretch and fold 3 times.

Proof 2-4h at room temp  and 45min in freezer **OR** 1h at room temp and 6-24h in fridge. Check with poke test.

Bake at 230C for 30 min with lid and 15-20min without lid.
