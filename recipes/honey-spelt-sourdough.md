---
author:
  name: King Arthur Baking
  source: "https://www.kingarthurbaking.com/recipes/honey-spelt-sourdough-bread-recipe"
title: Honey Spelt Sourdough Bread
tags:
  - baking
  - bread
---

# Honey Spelt Sourdough Bread

## Levain

- 99g spelt flour
- 113g water
- 1T starter

Mix night **before**.

---

## Dough

- 298g spelt flour
- 120g + 57g flour (177g)
- 227g water, lukewarm
- 28g butter, melted
- 43g honey
- 1.25t salt (8g)
- 0.5t instant yeast

Knead all with *levain* and 120g flour. Add rest of flour if needed.  
Rise until puffy (~1 hour).  
Form a log. Put in the loaf pan. Rise for 1 - 1.5 hours.

Bake for 25 - 30 minutes at 200C with a lid, then 5 minutes without a lid (internal temperature 87C).
