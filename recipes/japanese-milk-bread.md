---
author: 
  name: King Arthur Baking
  source: "https://www.kingarthurbaking.com/recipes/japanese-milk-bread-recipe"
title: Japanese Milk Bread
tags:
  - baking
  - bread
---

# Japanese Milk Bread

## Tangzhong

- 43g water
- 43g milk
- 14g flour

Combine in saucepan, cook until gel-like.

---

## Dough

- 298g bread flour
- 2T (14g) dry milk
- 50g sugar
- 1t (6g) salt
- 1T instant yeast
- 113g + **extra** milk
- 1 egg
- 57g butter, melted

Combine *tangzhong* with the rest. Knead until elastic (~15 minutes in stand mixer).
Shape into a ball. Rest until almost doubled in size in an oiled bowl (60 - 90 minutes).

Divide into four pieces. Fold each into a letter, roll into a log. Place seam side down in the loaf pan. Rest until puffy (40 - 50 minutes).

Brush with milk. Bake for 30 - 35 minutes at 175C (internal temperature 87C).
