---
author:
  name: "Claire Saffitz"
  source: "Desert Person/YT"
title: Poppy Seed Almond Cake
tags: 
  - baking
  - desert
---

# Poppy Seed Almond Cake

## Glaze

- 90g powdered sugar
- 57g orange juice
- 2t butter, melted
- 1/2t vanilla
- 1/2t almond

## Dough

- 225g sugar
- 17g poppy seeds
- 3/2t baking powder
- 1t salt
- 390g flour
- 360g milk
- 288g oil
- 3 eggs
- 3/2t vanilla
- 3/2t almond extract

Mix together and beat for 2min. Bake at 175C for 80-90min. 
