---
author: 
  name: Patrick Ryan
  source: "https://www.ilovecooking.ie/food-tv/masterclass-100-rye-sourdough/"
title: Rye Sourdough by Patrick Ryan
tags:
  - baking
  - bread
---

# Rye Sourdough by Patrick Ryan

## Dough

- 500g rye flour
- 10g salt
- 350g starter, rye
- 360g water

Bring together for 5 minutes. Proof in an **oiled** bowl for 2.5 hours.

Shape into a round. Proof in the toweled bowl for 2.5 hours.

Turn into a baking sheet. Dust with flour. Wait until surface cracks (a few minutes).  
Bake for 40 - 45 minutes at 230C with steam (1C/235g water).
