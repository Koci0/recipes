---
author: 
  name: King Arthur Baking
  source: "https://www.kingarthurbaking.com/recipes/sourdough-ciabatta-sandwich-rolls-recipe"
title: Sourdough Ciabatta Sandwich Rolls
tags:
  - baking
  - bread
---

# Sourdough Ciabatta Sandwich Rolls

## Dough

- 225g starter
- 283g water, lukewarm
- 170g milk, lukewarm
- 14g olive oil
- 1T (18g) salt
- 723g + 121g flour (844g)
- 2t instant yeast

Mix all using 723g flour. Knead adding up to 121g flour. Rise until doubled (~2 hours).

Pat into rectangle, cut 6 rolls. Rise until puffy (~30 minutes).

Spray with water and **optionally** sift flour on top.  
Bake for 10 minutes at 220C, then for 20 - 25 minutes at 190C.  
Cool in the oven with doors slightly opened.
