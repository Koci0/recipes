---
author:
  name: "Chef John"
  source: ""
title: Buttermilk biscuits
tags: 
  - baking
---

# Buttermilk biscuits

## Dough

- 280g flour
- 2t baking powder
- 1t salt
- 1/4t baking soda
- 100g butter, chilled and cubed
- 190ml buttermilk, cold (+ 2T for brushing)

Like pie dough. Press and fold into thirds 3 times. Cut in shape without twisting. Bake at 220C for 15min.
