---
author:
  name: "Claire Saffitz"
  source: "Desert Person/YT"
title: Claire's Oatmeal Cookies
tags: 
  - baking
  - sweet
---

# Claire's Oatmeal Cookies

## Pecan brittle

- 142g pecans
- 150g sugar
- 57g butter
- 1/2t baking soda
- 1/2t salt

Toast pecans. 
Make caramel from butter and sugar (with tiny bit of water). Mix pecans. Add baking soda and salt. Spread to harden.

## Dough

- 110g butter, browned and cooled
- 110g butter
- 173g flour
- 2t salt
- 1t baking soda
- 100g oats, milled
- 100g oats
- 75g dark brown sugar
- 50g sugar
- 2 eggs, cold
- 1T vanilla

Cream butters and sugars. Add eggs and vanilla, cream. Add dry. Add oats. Add *brittle*.  
Divide into balls and chill (overnight).

Bake at 180C for 16-20min.
